var page = require('webpage').create(),
    apiClient = require('webpage').create(),
    configs = JSON.parse(require('fs').read('./configs.json')),
    stage = null,
    phoneNumber = null,
    tzid = null;

apiClient.open('http://onlinesim.ru/api/getNum.php?service=yahoo&form=1&apikey=' + configs.onlineSimApi.key, function (status) {
    tzid = JSON.parse(apiClient.plainText).tzid;
    apiClient.open('http://onlinesim.ru/api/getState.php?tzid=' + tzid + '&apikey=' + configs.onlineSimApi.key, function () {
        console.log('First request is performing...');
        console.log(apiClient.plainText);
        var response = JSON.parse(apiClient.plainText)[0];
        console.log(response.response);
        if (response.response == 'TZ_NUM_WAIT') {
            phoneNumber = response.number;
            performRegistration();
        }
    });
});

function performRegistration() {
    page.settings.userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.3319.102 Safari/537.36';
    page.onConsoleMessage = function (msg) {
        console.log(msg);
        page.render(msg + '.png');
    };
    page.onLoadFinished = function () {
        console.log('Current page: ' + page.url);
        switch (resolvePageName(page.url)) {
            case 'index':
                handleIndexPage();
                break;
            case 'signIn':
                handleSignInPage();
                break;
            case 'registration':
                handleRegistrationPage();
                break;
            case 'phoneVerify':
                handlePhoneVerifyPage();
                break;
            case 'success':
                console.log('Registration has been performed');
                phantom.exit();
                break;
            case 'recaptcha':
                console.log('Fucking recaptcha');
                phantom.exit();
                break;
        }
    };
    page.open(configs.url);
}

// OTHER
function resolvePageName(url) {
    if (url.indexOf('https://www.yahoo.com') >= 0) {
        return stage = 'index';
    } else if (url.indexOf('https://login.yahoo.com/config/login') >= 0) {
        return stage = 'signIn';
    } else if (url.indexOf('https://login.yahoo.com/account/create') >= 0) {
        return stage = 'registration';
    } else if (url.indexOf('https://login.yahoo.com/account/challenge/phone-verify') >= 0) {
        return 'phoneVerify'
    } else if (url.indexOf('https://login.yahoo.com/account/create/success') >= 0) {
        return stage = 'success'
    } else if (url.indexOf('https://login.yahoo.com/account/challenge/recaptcha') >= 0) {
        return 'recaptcha';
    }
}

// PAGE HANDLERS
function handleIndexPage() {
    page.evaluate(function () {
        document.getElementById('uh-signin').click();
    });
}
function handleSignInPage() {
    page.includeJs(configs.jqueryCdn, function () {
        page.evaluate(function () {
            $('.mbr-login-create-account').find('a')[0].click();
        });
    });
}
function handleRegistrationPage() {
    page.includeJs(configs.jqueryCdn, function () {
        page.evaluate(function (phoneNumber) {
            var registerForm = $('#regform');
            registerForm.find('[name=firstName]').val('Ivan');
            registerForm.find('[name=lastName]').val('Sergeev');
            registerForm.find('[name=yid]').val('Ivanovbvdfvvdfvser4dv');
            registerForm.find('[name=password]').val('Art220788arty');
            registerForm.find('[name=shortCountryCode]').val('UA');
            registerForm.find('[name=phone]').val(phoneNumber.substr(2));
            registerForm.find('[name=mm]').val('2');
            registerForm.find('[name=dd]').val('12');
            registerForm.find('[name=yyyy]').val('1980');
            registerForm.find('[name=freeformGender]').val('Male');
            document.getElementById('reg-submit-button').click();
        }, phoneNumber);
    });
}
function handlePhoneVerifyPage() {
    if (stage == 'registration') {
        console.log('phoneVerify1' + phoneNumber);
        page.includeJs(configs.jqueryCdn, function () {
            page.evaluate(function (phoneNumber) {
                $(function () {
                    $('[name=sendCode]')[0].click();
                });
            }, phoneNumber);
        });
        stage = 'phoneVerify';
    } else if (stage == 'phoneVerify') {
        page.render('Trying to verify number.png');
        setTimeout(function tic() {
            apiClient.open('http://onlinesim.ru/api/getState.php?tzid' + tzid + '&apikey=' + configs.onlineSimApi.key, function () {
                var response = JSON.parse(apiClient.plainText)[0];
                for(var key in response) {
                    console.log(key);
                    console.log(response[key])
                }
                if (response.msg) {
                    page.evaluate(function (code) {
                        $('input[name=code]').val(code);
                        $('button[name=verifyCode]')[0].click();
                    }, response.msg);
                } else {
                    setTimeout(tic, 1000);
                }
            });
        }, 1000);
        page.render('dfvdfv.png');
    }
}
